#! /bin/sh

mkfs.btrfs -L nixos /dev/vda
mount /dev/vda /mnt

nixos-generate-config --root /mnt

cp ./configuration.nix /mnt/etc/nixos/configuration.nix

nixos-install

# need to set password
passwd

#need to make sure ssh daemon is running
systemctl restart sshd

systemctl status sshd
