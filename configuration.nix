{ config, pkgs, ...}:

{
  imports =
    [
      ./hardware-configuration.nix
    ];

  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  boot.loader.grub.device = "/dev/vda";

  services.openssh.enable = true;
  services.openssh.passwordAuthentication = false;


  networking = {
    # wireless.enable = true;     # Enables wireless support via wpa_supplicant.
    # networkmanager.enable = true; # Enable NetworkManager
    firewall.enable = true;
    firewall.allowedTCPPorts = [22 80];
    firewall.allowedUDPPorts = [22 80];
    # allow containers to show up on lan/wan
    nat = {
      enable = true;
      # (nixos-container command line tool creates containers named ve-containername)
      internalInterfaces = ["ve-+"];
      externalInterface = "ens3";
    };
  };

  users.extraUsers.nixos = {
    isNormalUser = true;

    extraGroups = ["wheel"];
    openssh.authorizedKeys.keys =
      [
"ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDIYLiGfPusOTyGCQhuiFd0W8WXYiS8hBEaYNl2uJvARyXe6ZsePmirIME/sBgKXXDT1hfPBcyBPCppdqX+Usyh0+h2ahHVKrtaD46cjpoC0siqd/LwPwtlw1qxurecPpqtae1EoZpIGlAFnfVM9uPO8NMcBcKWmZuMN6iyMk/JPRplui3pU8ZMqbxCqukqY377fdvkBVlRouyM0QBSUByyBe/b662OXcCyuqBkfkSyvdgk/QgLfUfySjIAzOc7Sql8DKRaBQVHpvH6p3+nDcTRRTcnrvm92cySC9Mi3FhZlVjUFVnh13KcoHsyWqGA6BdPjxLsHpzLWj1CA5y7dYX5aKsJ5cBx0oOUXSapF2wM16EiruglAan6lOkL2P7ZlUZb9RPKx+bqU3KMbfbSRB47reZ5SkvYzhDSlT8wBVp985MOAHTt3i52scL/gtgVE9q9iRM3nZ9wUq/6mJ98zoxGVIh92kVJgJ7ny4+i/hZW6H25K4OXHOlddQ4xuHTdzQo3ZKH7I37c8w6WIxZSBL7E1WzktqLEK8cYbhPBVk5Wo18sIPTwj6Gu8JbVDRjGtKfL4Vn4DG1ruXkK/qcsDeFd98pUD5TX/wEMCX294DAbbt2XBXZSu5LrueEqoO39pg7A8higD5JY6yWXh+Psfm+Efe0Jp1fh982MNHK0fEyHsQ== darklordvadermort@gmail.com"
      ];

  };

  system.stateVersion = "20.03";
}
